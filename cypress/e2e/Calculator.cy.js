
import CalculationLocators from "./CalculatorLocators/calculationLocators"
import EqualLocator from "./CalculatorLocators/equalLocator"
import EraseLocators from "./CalculatorLocators/eraseLocator"
import NumberLocators from "./CalculatorLocators/numberLocators"
import ResultLocator from "./CalculatorLocators/resultLocator"


const numbers = new NumberLocators
const calcualtion = new CalculationLocators
const erase = new EraseLocators
const result = new ResultLocator
const equal = new EqualLocator

describe('Testing Calculator', () => {
  beforeEach(() => {
    cy.visit('cypress\\Calculator\\calculator.html')
  })
  it('First test case of my calculator', () => {
    cy.get('#result').should('have.value', '0')
    cy.get('body').should('be.visible')
  })
  it('Testing if every number is working', () => {
    numbers.pressAllNumbers()
    result.Result().should('have.value', '1234567890')
  })
  it('Testing if the addition button works', () => {
    numbers.pressAllNumbers()
    calcualtion.Plus()
    numbers.pressAllNumbers()
    equal.Equal()
    result.Result().should('have.value', '2469135780')
  })
  it('Testing if the subtract button works', () => {
    numbers.pressAllNumbers()
    calcualtion.Minus()
    numbers.pressAllNumbers()
    equal.Equal()
    result.Result().should('have.value', '0')
  })
  it('Testing if the multiply button works', () => {
    numbers.pressAllNumbers()
    calcualtion.Multiplication()
    numbers.pressAllNumbers()
    equal.Equal()
    result.Result().should('have.value', '1524157875019052000')
  })
  it('Testing if the divide  button is working', () => {
    numbers.pressAllNumbers()
    calcualtion.Dividing()
    numbers.pressAllNumbers()
    equal.Equal()
    result.Result().should('have.value', '1')
  })
  it('Testing if the erase button is working', () => {
    numbers.pressAllNumbers()
    erase.Erase()
    result.Result().should('have.value', '0')
  })
  it('Testing if the equal button is working', () => {
    numbers.pressAllNumbers()
    equal.Equal()
    result.Result().should('have.value', '0')
  })

})