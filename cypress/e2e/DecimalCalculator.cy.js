import CalculationLocators from "./CalculatorLocators/calculationLocators"
import EqualLocator from "./CalculatorLocators/equalLocator"
import EraseLocators from "./CalculatorLocators/eraseLocator"
import NumberLocators from "./CalculatorLocators/numberLocators"
import ResultLocator from "./CalculatorLocators/resultLocator"


const numbers = new NumberLocators
const calcualtion = new CalculationLocators
const erase = new EraseLocators
const result = new ResultLocator
const equal = new EqualLocator

describe('Testing Calculator', () => {
  beforeEach(()=>{
    cy.visit('cypress\\Calculator\\calculator.html')
  })
  it('Second test case of my calculator', () => {
    result.Result().should('have.value','0')
    cy.get('body').should('be.visible')
  })
  it('Testing if the addition button works with decimal numbers', () => {
    numbers.Five(),calcualtion.Decimal(),numbers.Two()
    calcualtion.Plus()
    numbers.Five(),calcualtion.Decimal(),numbers.Three()
    equal.Equal()
    result.Result().should('have.value','10.5')
  })
  it('Testing if the subtract button works with decimal numbers', () => {
    numbers.Seven(),calcualtion.Decimal(),numbers.Zero()
    calcualtion.Minus()
    numbers.Six(),calcualtion.Decimal(),numbers.Five()
    equal.Equal()
    result.Result().should('have.value','0.5')
  })
  it('Testing if the multiply button works with decimal numbers', () => {
    numbers.Two(),calcualtion.Decimal(),numbers.Five()
    calcualtion.Multiplication()
    numbers.Two(),calcualtion.Decimal(),numbers.Five()
    equal.Equal()
    result.Result().should('have.value','6.25')
  })
  it('Testing if the devide button works with decimal numbers', () => {
    numbers.Two(),calcualtion.Decimal(),numbers.Five()
    calcualtion.Dividing()
    numbers.Two(),calcualtion.Decimal(),numbers.Five()
    equal.Equal()
    result.Result().should('have.value','1')
  })
  it('Testing negative numbers', () => {
    calcualtion.Minus()
    numbers.Two(),numbers.Zero()
    calcualtion.Plus()
    numbers.One(),numbers.Zero()
    equal.Equal()
    result.Result().should('have.value','-10')
  })
})