class NumberLocators {
    Zero(){
  return cy.get('.keys > [value="0"]').click()
    }
    One(){
        return cy.get('[value="1"]').click()
    }
    Two(){
        return cy.get('[value="2"]').click()
    }
    Three(){
        return cy.get('[value="3"]').click()
    }
    Four(){
        return cy.get('[value="4"]').click()
    }
    Five(){
        return cy.get('[value="5"]').click()
    }
    Six(){
        return cy.get('[value="6"]').click()
    }
    Seven(){
        return cy.get('[value="7"]').click()
    }
    Eight(){
        return cy.get('[value="8"]').click()
    }
    Nine(){
        return cy.get('[value="9"]').click()
    }
    pressAllNumbers(){
 return cy.get('[value="1"]').click(),
        cy.get('[value="2"]').click(),
        cy.get('[value="3"]').click(),
        cy.get('[value="4"]').click(),
        cy.get('[value="5"]').click(),
        cy.get('[value="6"]').click(),
        cy.get('[value="7"]').click(),
        cy.get('[value="8"]').click(),
        cy.get('[value="9"]').click(),
        cy.get('.keys > [value="0"]').click()
       
    }
}

export default NumberLocators