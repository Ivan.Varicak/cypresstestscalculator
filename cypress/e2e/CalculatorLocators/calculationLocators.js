class CalculationLocators {
    Plus(){
        return cy.get('[value="+"]').click()
    }
    Minus(){
        return cy.get('[value="-"]').click()
    }
    Multiplication(){
        return cy.get('[value="*"]').click()
    }
    Dividing(){
        return cy.get('[value="/"]').click()
    }
    Decimal(){
        return cy.get('.decimal').click()
    }
    

}
export default CalculationLocators