# CypressTestsCalculator



## Getting started
Hello everyone
In order to run the Cypress test on your machine you must do the following:

!!!First of all, you must have NODE.JS installed on your machine, if you don't have it, you must first install it!!!
Here we will go through the installation of NODE.JS:
Go website https://nodejs.org/en/ and download the recommended version of NODE.JS
Run the installer (the .msi file you downloaded in the previous step.)
Follow the prompts in the installer (Accept the license agreement, click the NEXT button a bunch of times and accept the default installation settings).
Restart your computer.
Now that we have NODE.JS installed let's get back to running our Cypress test
1.Create an empty folder on your desktop

2.Go to my git repository (https://gitlab.com/Ivan.Varicak/cypresstestscalculator) and click the CLONE button

3.A window will open with more options, copy the link where it says Clone with HTTPS (https://gitlab.com/Ivan.Varicak/cypresstestscalculator.git)

4.After that, go back to the folder you opened right-click on the white background in the folder

5.In the options that appear, you need to click GitBashHere or OpenTerminal depending on which system you are using
Ps.If you use windows and do not have a terminal installed, do the following
Go to the website https://git-scm.com/download/win and download the version of git that corresponds to your machine
Installation is very easy, with a few clicks on next and accept you will be ready to use the terminal on your machine
Ps.some will have to restart the computer

6.Once you have clicked and opened the terminal (the black window should be open)

7.There you type git clone (link from my repository)and press enter

8.Wait for the project to download. A 'cypresstestscalculator' folder will be created in your folder and close terminal

9.Open cypresstestscalculator folder again open terminal in that folder and type npm install cypress wait for node_modules folder to be created 

10.Type in npm run chromeTest and Cypress will open 

11.Select E2E testing (It should say Configured)

12.Select browser and click on Start E2E testing (in browser that you selected)

13.Click on Calculator.cy.js and test should start





